//
//  RepositoryHelper.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 27/02/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

import RealmSwift

public class DatabaseHelper {
    
    static var db: Realm?
    class func getDBInstance() -> Realm {
        
        let documentDirectory = DatabaseHelper.getDocumentPathDirectory()! + "/DataBase"
        let language = "en"
        let fileManager = FileManager.default
        if (!fileManager.fileExists(atPath: documentDirectory)) {
            do {
                try fileManager.createDirectory(atPath: documentDirectory, withIntermediateDirectories: true, attributes: nil)
                
            } catch {
                print ("File Not Found")
            }
        }
        print(documentDirectory)
        let url = documentDirectory + String(format:"/purevpnDB_%@.realm",language)
        DatabaseHelper.db = try! Realm(configuration:Realm.Configuration(
            fileURL:URL(string:url),
            inMemoryIdentifier: nil,
            syncConfiguration: nil,
            encryptionKey: nil,
            readOnly: false,
            schemaVersion: 0,
            deleteRealmIfMigrationNeeded: true,
            objectTypes: nil))
        
        return DatabaseHelper.db!
    }
    
  public  class func addDataModelToDB(object: AnyObject) {
        let dbInstance = DatabaseHelper.getDBInstance()
        try! dbInstance.write {
            dbInstance.add(object as! Object, update: true)
        }
    }
    
    private func addDataToDatabase(object: Object)  {
        let dbInstance = DatabaseHelper.getDBInstance()
        try! dbInstance.write {
            dbInstance.add(object, update: true)
        }
    }
    
  public  func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    
   public class func insert<T>(object: [T]) {
        let dbInstance = DatabaseHelper.getDBInstance()
        try? dbInstance.write {
            for dbItem in object{
              dbInstance.add(dbItem as! Object , update: true)
            }
        }
    }
    
   public class func get<T>(object: T.Type, withPredicate: NSPredicate?) -> [T]? {
        
        let dbInstance = DatabaseHelper.getDBInstance()
        if(withPredicate == nil){
            print(dbInstance.objects(object as! Object.Type))
            return dbInstance.objects(object as! Object.Type).toArray(type: object)
        }else{
            print(dbInstance.objects(object as! Object.Type))
            return dbInstance.objects(object as! Object.Type).filter(withPredicate!).toArray(type: object)
        }
    }
    
   public class func delete<T>(object: T, withPredicate: NSPredicate?) {
        let dbInstance = DatabaseHelper.getDBInstance()
        if(withPredicate == nil){
            try! dbInstance.write {
                dbInstance.delete(object as! Object)
            }
        }
    }
    
   public class func deleteAll() {
        let dbInstance = DatabaseHelper.getDBInstance()
        try! dbInstance.write {
            dbInstance.deleteAll()
        }
    }
    
   public class func deleteFromDb(object: Object) {
        let dbInstance = DatabaseHelper.getDBInstance()
        try! dbInstance.write {
            dbInstance.delete(object)
        }
    }
}
extension DatabaseHelper{
    
   class func getDocumentPathDirectory() -> String? {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        //let sourcePath =  documentsPath.appending("/ApiUrls.plist")
        return documentsPath as String
        
    }
}

extension Results  {
    func toArray<T>(type: T.Type) -> [T] {
        return compactMap { $0 as? T }
    }
}

extension Object {
    func toDictionary() -> [String:AnyObject] {
        let properties = self.objectSchema.properties.map { $0.name }
        var dicProps = [String:AnyObject]()
        for (key, value) in self.dictionaryWithValues(forKeys: properties) {
            if let value = value as? ListBase {
                dicProps[key] = value.toArray() as AnyObject
            } else if let value = value as? Object {
                dicProps[key] = value.toDictionary() as AnyObject
            } else {
                dicProps[key] = value as AnyObject
            }
        }
        return dicProps
    }
}

extension ListBase {
    func toArray() -> [AnyObject] {
        var _toArray = [AnyObject]()
        for i in 0..<self._rlmArray.count {
            let obj = unsafeBitCast(self._rlmArray[i], to: Object.self)
            _toArray.append(obj.toDictionary() as AnyObject)
        }
        return _toArray
    }
}
