//
//  BaseRepositoryModel.swift
//  Repository-iOS
//
//  Created by Muhammad kumail on 27/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation
import ObjectMapper
import ObjectMapper_Realm
import RealmSwift

public class BaseRepositoryModel :Object ,Mappable
{
    required convenience public init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        
    }
}
