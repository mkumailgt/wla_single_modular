//
//  CountryRepositoryModel.swift
//  Repository-iOS
//
//  Created by Muhammad kumail on 28/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import ObjectMapper_Realm

#if os(iOS)
    import Core_iOS
#else
    import Core_Mac
#endif

@objcMembers class ServerListingRepositoryModel: BaseRepositoryModel , CountryModelProtocol {
    
   dynamic var countryId: Int = 0
   dynamic var name: String?
   dynamic var iso_code: String?

    
    override public static func primaryKey() -> String?
    {
        return #keyPath(ServerListingRepositoryModel.countryId)
    }

    required convenience init?(map: Map) {
        self.init()
    }

    override public func mapping(map: Map) {
        countryId               <- map["countryId"]
        name                    <- map["name"]
        iso_code                <- map["iso_code"]
    }
}
