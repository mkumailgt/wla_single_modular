//
//  LocationRepositoryModel.swift
//  WLA_iOS
//
//  Created by Muhammad kumail on 22/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import ObjectMapper_Realm

#if os(iOS)
    import Core_iOS
#else
    import Core_Mac
#endif

@objcMembers class LocationRepositoryModel: BaseRepositoryModel,LocationModelProtocol{
    
    dynamic var Id: Int = 0
    //dynamic var success: Bool?
    dynamic var ip: String?
    dynamic var city: String?
    dynamic var country: String?
    dynamic var iso2: String?
    dynamic var latitude: String?
    dynamic var longituge: String?
    dynamic var isp: String?
    dynamic var organization: String?
    
    override static func primaryKey() -> String?
    {
        return #keyPath(LocationRepositoryModel.Id)
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        Id              <- map["Id"]
        ip              <- map["ip"]
        city            <- map["city"]
        country         <- map["country"]
        iso2            <- map["iso2"]
        latitude        <- map["latitude"]
        longituge       <- map["longituge"]
        isp             <- map["isp"]
        organization    <- map["organization"]
    }
}
