//
//  LocationRepository.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 27/02/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation
import RealmSwift

#if os(iOS)
    import Core_iOS
#else
    import Core_Mac
#endif

public class LocationRepository: BaseRepository , LocationRepositoryProtocol {

   public override init() {}
    
    public func getLocationById(Id: Int) -> LocationModel? {
        
        let predicate : NSPredicate = NSPredicate(format: "%K == %@", argumentArray: [#keyPath(LocationRepositoryModel.Id),Id])
        let locationRepoModel = self.get(model: LocationRepositoryModel.self, predicate: predicate)
        if let repoModel = locationRepoModel{
            let mainModel = mapRepoModelToMainModel(fromModel: repoModel, ToModel: LocationModel.self)?.first
            return mainModel
        }
        return nil
    }
    
    public func addlocation(object: LocationModel) {
        
        let locationModel = convertMainModelJson(fromModel: [object], toModel: LocationRepositoryModel.self)
        self.insert(modelObject: locationModel!)
    }
    
}
