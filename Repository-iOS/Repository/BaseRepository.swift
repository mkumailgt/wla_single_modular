//
//  BaseRepository.swift
//  WLA_iOS
//
//  Created by Muhammad kumail on 22/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import ObjectMapper_Realm

public class BaseRepository :NSObject{
    
    public func get<T:Object>(model:T.Type,predicate:NSPredicate?) -> [T]?{
        
        return DatabaseHelper.get(object: model, withPredicate: predicate)
    }
    
    public func insert<T:Object>(modelObject:[T]){
        DatabaseHelper.insert(object: modelObject)
    }
    
    public func delete<T:Object>(object:T ,predicate:NSPredicate?){
        DatabaseHelper.delete(object: object, withPredicate: predicate)
    }
    
    
    internal func mapRepoModelToMainModel<RealmModel:Object,T:Decodable>(fromModel:[RealmModel],ToModel:T.Type) -> [T]?{
        
        var arrayModel :[T] = []
        
        for obj in fromModel {
            let jsonSchema = obj.toDictionary()
            do{
                if(jsonSchema.keys.count == 0){
                    return nil
                }
                
                let jsonData = try JSONSerialization.data(withJSONObject: jsonSchema, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do{
                    let decodedModel = try decoder.decode(ToModel, from: jsonData)
                    arrayModel.append(decodedModel)
                    // return decodedModel
                }catch {
                    print (error.localizedDescription)
                }
            }catch{
                print (error.localizedDescription)
            }
            
            
        }
        
        return arrayModel
    }
    
    internal func convertMainModelJson<T:Encodable,RealmModel:BaseRepositoryModel>(fromModel:[T] , toModel:RealmModel.Type) -> [RealmModel]? {
         var arrayModel :[RealmModel] = []
        do
        {
            for model in fromModel {
                let encode  = try  JSONEncoder().encode(model)
                let json = try JSONSerialization.jsonObject(with: encode, options: []) as? [String : Any]
                let model = Mapper<RealmModel>()
                let mappedModel = model.map(JSONObject:json)
                arrayModel.append(mappedModel!)
            }
            
             return arrayModel
        }catch{
            print("Cant map to Repo Model and Exception throws")
        }
        
        return nil
        
    }
}
