//
//  ServerListingRepository.swift
//  Repository-iOS
//
//  Created by Muhammad kumail on 29/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation
import RealmSwift

#if os(iOS)
    import Core_iOS
#else
    import Core_Mac
#endif

public class ServerListingRepository: BaseRepository, ServerListingRepositoryProtocol {
  
    public override init() {}
    
    public func getCountries() -> [CoutryModel]? {
        let locationRepoModel = self.get(model: ServerListingRepositoryModel.self, predicate: nil)
        if let repoModel = locationRepoModel{
            let mainModel = mapRepoModelToMainModel(fromModel: repoModel, ToModel: CoutryModel.self)
            return mainModel
        }
        return nil
    }
    
    public func addCountries(object: [CoutryModel]) {
       
        guard let countryModel = convertMainModelJson(fromModel: object, toModel: ServerListingRepositoryModel.self) else { return }
        self.insert(modelObject: countryModel)
        
    }
}


