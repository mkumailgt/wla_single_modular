//
//  LocationService.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 27/02/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

#if os(iOS)
    import Core_iOS
#else
    import Core_Mac
#endif

public class LocationService: LocationServiceProtocol {
    
    public  var locationNetwork : LocationNetworkProtocol
    public  let locationRepository : LocationRepositoryProtocol
    
    //Injecting Dependencies Network Protocol and Repository Protocol
    
    public init(network : LocationNetworkProtocol , repository : LocationRepositoryProtocol) {
        self.locationNetwork = network
        self.locationRepository = repository
    }
    
    // Implementing Service Protocol Methods
    
    public func getLocation(response:@escaping (LocationModel?) -> Void) {
        
        let getDataFromDB = locationRepository.getLocationById(Id: 1)

        if getDataFromDB != nil {
            response(getDataFromDB)
        }
        else
        {
            locationNetwork.getLocation(parameters:nil) { (responseModel) in
                
                
                guard let locationModel = responseModel else {
                    response(nil)
                    return
                }
                response(locationModel)
                self.locationRepository.addlocation(object: locationModel)
            }
        }
    }
}
