//
//  ServerListingService.swift
//  Service-iOS
//
//  Created by Muhammad kumail on 28/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

#if os(iOS)
    import Core_iOS
#else
    import Core_Mac
#endif

public class ServerListingService  {
    
    let vpnService : VPNServiceProtocol
    let serverListingRepo : ServerListingRepositoryProtocol
    public init(vpnService : VPNServiceProtocol , serverListingRepo : ServerListingRepositoryProtocol ) {
        self.vpnService = vpnService
        self.serverListingRepo = serverListingRepo
    }
    
    public func getCountriesFromSDK(response:@escaping ([CoutryModel]?) -> Void)  {
        
        
        let getDataFromDB = serverListingRepo.getCountries()

        if getDataFromDB != nil {
            response(getDataFromDB!)
        }
        else
        {
            self.vpnService.getCountries { (responseModel) in
                
//                guard let countryMdl = responseModel else {
//                    response(nil)
//                    return
//                }
                response(responseModel!)
                self.serverListingRepo.addCountries(object: responseModel!)
            }
        }
        

    }
    
}
