//
//  SmartConnectService.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 04/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

#if os(iOS)
    import Core_iOS
#else
    import Core_Mac
#endif

public class UserService: UserServiceProtocol {
    
    var userNetwork : UserNetworkProtocol
    
    public init(userNetwork : UserNetworkProtocol) {
        
        self.userNetwork = userNetwork
    }
    
    public func login(userName:String,password:String,response: @escaping (LoginModel?) -> Void) {
        
        var loginParams : Dictionary<String,Any> = Dictionary <String ,Any> ()
        loginParams.updateValue(Utilities.deviceType!, forKey: Constants.CommonParameters.DeviceType)
        loginParams.updateValue(-1, forKey: Constants.CommonParameters.DeviceId)
        loginParams.updateValue(userName, forKey: Constants.CommonParameters.Username)
        loginParams.updateValue(password, forKey: Constants.CommonParameters.Password)
        
        userNetwork.login(paramters:loginParams) { (apiResponseModel) in
            
            print(self.userNetwork.apiHttpResponse)
            
            guard let loginMdl = apiResponseModel else {
                response(nil)
                return
            }
            
            if (loginMdl.code == self.userNetwork.successCode){
                
                self.saveLoginInfo(loginModel: loginMdl)
                response(loginMdl)
                
            }else{
                response(nil)
            }
        }
    }
    
    
    public func getUserProfile(completion: @escaping (UserProfileModel?) -> Void) {
        
        var profileParams : [String:Any] = [String:Any]()
        profileParams.updateValue("1", forKey: Constants.CommonParameters.DeviceId)
        profileParams.updateValue("mac", forKey: Constants.CommonParameters.DeviceType)
        profileParams.updateValue("0e4617470f40fce31a8c73c905c3daf9", forKey: Constants.CommonParameters.Token)
        profileParams.updateValue("2019-03-12%2012%3A59%3A02", forKey: Constants.CommonParameters.DateTime)
        profileParams.updateValue("1775861702b9af397c3a0773897d4ae6", forKey: Constants.CommonParameters.Signature)
        profileParams.updateValue("1295395", forKey: Constants.CommonParameters.ClientId)
        
        userNetwork.getUserProfile(paramters:profileParams) { (apiResponseModel) in
            
            guard let profileModel = apiResponseModel else {
                completion(nil)
                return
            }
            
            if (profileModel.code == self.userNetwork.successCode){
                completion(profileModel)
            }else{
                completion(nil)
            }
            
            print(self.userNetwork.apiHttpResponse)
        }
    }
    
    
    private func saveLoginInfo(loginModel:LoginModel){
        
        Utilities.accessToken = loginModel.token ?? ""
        Utilities.clientId = loginModel.client_id ?? 0
    }
}
