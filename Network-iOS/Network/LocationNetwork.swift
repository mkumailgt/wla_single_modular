//
//  LocationNetwork.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 27/02/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

#if os(iOS)
    import Core_iOS
#else
    import Core_Mac
#endif

public class LocationNetwork: BaseNetwork , LocationNetworkProtocol {
    
    public override init() {}
    
    public func getLocation(parameters:[String:Any]? , response: @escaping (LocationModel?) -> Void) {
        
        self.get(url: ApiAllUrls.URL_LOCATION, parameters: parameters, headers: nil, mappingModel: LocationModel.self) { (responseModel, error) in
            if error != nil{
                response (nil)
            }else{
                response(responseModel)
            }
        }
    }
}

