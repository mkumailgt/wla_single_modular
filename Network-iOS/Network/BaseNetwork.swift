//
//  BaseNetwork.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 04/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

#if os(iOS)
    import Core_iOS
#else
    import Core_Mac
#endif

public class BaseNetwork: BaseNetworkProtocol {
   
    public var apiUrl: String = ""
    public var apiEndPoint: String = ""
    public var apiHttpBody: String = ""
    public var apiParameters: [String : Any] = ["" : ""]
    public var apiHttpHeader: [String : String]? = nil
    public var apiHttpResponse: String = ""
    public var apiHttpMethod: String = ""
    public var apiHttpResponseCode:Int = 0
    public var apiErrorCode: String = ""
    public var apiErrorMessage: String = ""
    public var successCode: Int = 0
    
    // GET Method
    func get<T:Decodable>(url:String,parameters:[String:Any]?,headers:[String:String]?, mappingModel:T.Type,completionHandler:@escaping (T?,Swift.Error?) -> Void){
        
        setProperties(url: url, parameter: parameters,headers:headers)
        
        WebRequestHelper.get(url: self.apiUrl,parameters: self.apiParameters, headers: self.apiHttpHeader){ (responseObject) in
            
            self.setApiHttpProperties(responseObject: responseObject)
            self.trackApi(responseObject: responseObject)
            
            if responseObject.isFailure!{
                completionHandler(nil,responseObject.error)
            }
            
            if responseObject.isSuccess! {
                if let response = responseObject.data{
                    let parsedModel = self.MapModel(responseFromApi: response, mappingModel: mappingModel)
                    completionHandler(parsedModel,nil)
                }
            }
        }
    }
    
    // POST Method
    func post<T:Decodable>(url:String,parameters:[String:Any]?,headers:[String:String]?,mappingModel:T.Type,completionHandler:@escaping (T?,Swift.Error?) -> Void){
        
        setProperties(url: url, parameter: parameters,headers:headers)
        
        WebRequestHelper.post(url: self.apiUrl, parameters: self.apiParameters, headers: self.apiHttpHeader){ (responseObject) in
            
            self.setApiHttpProperties(responseObject: responseObject)
            
            self.trackApi(responseObject: responseObject)
            
            if responseObject.isFailure!{
                completionHandler(nil,responseObject.error)
            }
            
            if responseObject.isSuccess!{
                if let response = responseObject.data{
                    let parsedModel = self.MapModel(responseFromApi: response, mappingModel: mappingModel)
                    completionHandler(parsedModel,nil)
                }
            }
        }
    }
    
    func setApiHttpProperties (responseObject :WebResponseModel) {
        let jsonString = String(data: responseObject.data!, encoding: .utf8)
        self.apiHttpResponse = jsonString ?? ""
        self.apiHttpResponseCode = responseObject.response?.statusCode ?? 0
    }
    
    func trackApi(responseObject : WebResponseModel){
        
        let responseObj = responseObject.result
        if let headerJSON = responseObj["header"] as? Dictionary<String, AnyObject>{
            var code :Int = 0
            if let Hcode = headerJSON["code"] as? String {
                code = Int(Hcode)!
            } else if let numberV = headerJSON["code"] as? NSNumber {
                print("NumberHeader")
                code = numberV.intValue
            }
            //let code = headerJSON["code"] as! Int
            let message = headerJSON["message"] as! String
            print(code,message)
            if (code != self.successCode){
                // send Api Error
                
            }else{
                //send Api Success Error Event
            }
        }else{
            if let success = responseObject.isSuccess{
                if(success){
                    // Api Success
                }
                else{
                    //Api Failour
                }
            }
        }
    }
    
    func MapModel<T:Decodable> (responseFromApi : Data?,mappingModel:T.Type) -> T?{
        
        if let response = responseFromApi{
            let decoder = JSONDecoder()
            do{
                let decodedModel = try decoder.decode(mappingModel, from: response)
                return decodedModel
            }catch {
                print (error.localizedDescription)
            }
        }
        return nil
    }
    
    func setProperties (url:String,parameter:[String:Any]?,headers:[String:String]?){
        
        self.apiUrl = url
        self.apiParameters = parameter ?? ["":""]
        self.apiHttpHeader = headers ?? ["XPSK":"FGtbAXMT4QBBLQj97jvP"]
    }
}


