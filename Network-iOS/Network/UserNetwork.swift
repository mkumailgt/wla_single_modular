//
//  SmartConnectNetwork.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 04/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

#if os(iOS)
    import Core_iOS
#else
    import Core_Mac
#endif

public class UserNetwork: BaseNetwork , UserNetworkProtocol   {
    
    public override init() {}
    
    public func login(paramters: [String : Any], response: @escaping (LoginModel?) -> Void) {
        
        self.apiParameters = paramters
        
        self.post(url: ApiAllUrls.URL_LOGIN, parameters: paramters, headers: nil, mappingModel: ApiEnvelope<LoginModel>.self){(responseModel,error) in
            
            var loginModel = responseModel?.body
            loginModel?.code = responseModel?.header?.code
            loginModel?.message = responseModel?.header?.message
            
            response(loginModel)
        }
    }
    
    public func getUserProfile(paramters: [String : Any], response: @escaping (UserProfileModel?) -> Void) {
        
        self.apiParameters = paramters
        
        self.get(url:ApiAllUrls.URL_GET_USER, parameters: paramters, headers: nil, mappingModel: ApiEnvelope<UserProfileObjectModel>.self){(responseModel,error) in
            
            let userModelObject = responseModel?.body
            var userModel = userModelObject?.profile_data
            userModel?.code = responseModel?.header?.code
            userModel?.message = responseModel?.header?.message
            
            response(userModel)
        }
    }
}
