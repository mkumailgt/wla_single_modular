//
//  AppDelegate.swift
//  Presentation
//
//  Created by Taha Siddiqui on 3/21/19.
//  Copyright © 2019 Taha Siddiqui. All rights reserved.
//

import Cocoa
import Swinject
import SwinjectAutoregistration
import Core_Mac
import Network_Mac
import Service_Mac
import Repository_Mac
import AtomSDK


@NSApplicationMain

class AppDelegate: NSObject, NSApplicationDelegate {

     let dependencyContainer = Container()
     let atomConfig = AtomConfiguration()
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


    func registerDependencies(){
        
        viewModelDependencies()
        serviceDependencies()
        atomConfig.secretKey = "dab9cf530d2932f1bfe5572fcaa9fa3dc7bd32fb";
        atomConfig.vpnInterfaceName = "WLA";
        AtomManager.sharedInstance(with: atomConfig)
        AtomManager.sharedInstance()?.uuid = "mkumailgt@gmail.com"
        
    }
    
    func viewModelDependencies(){
        dependencyContainer.autoregister(LocationViewModel.self, initializer: LocationViewModel.init)
        dependencyContainer.autoregister(LoginViewModel.self, initializer: LoginViewModel.init)
    }
    
    func serviceDependencies(){
        dependencyContainer.autoregister(LocationNetworkProtocol.self, initializer: LocationNetwork.init)
        dependencyContainer.autoregister(LocationRepositoryProtocol.self, initializer: LocationRepository.init)
        dependencyContainer.autoregister(LocationServiceProtocol.self, initializer: LocationService.init)
        dependencyContainer.autoregister(UserNetworkProtocol.self, initializer: UserNetwork.init)
        dependencyContainer.autoregister(UserServiceProtocol.self, initializer: UserService.init)
    }
    
    func getUUID() -> String {
        let applicationUUID = UUID().uuidString
        if (UserDefaults.standard.string(forKey: "UUID") == nil){
            UserDefaults.standard.set(applicationUUID, forKey: "UUID")
        }
        return UserDefaults.standard.string(forKey: "UUID") ?? ""
    }
}

