//
//  LoginViewController.swift
//  WLA_RND_POC
//
//  Created by Taha Siddiqui on 3/18/19.
//  Copyright © 2019 Taha Siddiqui. All rights reserved.
//

import Cocoa
import Core_Mac

class LoginViewController: BaseViewController {

    @IBOutlet weak var lblUserName: NSTextField!
    
    @IBOutlet weak var lblPassword: NSTextField!
    
    var loginViewModel :LoginViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.lblUserName.stringValue = "purevpn0d4037095"
        self.lblPassword.stringValue = "pvpn12345"
        
        loginViewModel = referenceContainer.resolve(LoginViewModel.self)
        // Do view setup here.
        
        loginViewModel.client_id.bind{ (clientID) in
            if clientID != 0 {
                print(clientID)
                self.performCustomSegue(identifier: Constants.customSegueIdentifiers.dashboardViewSegue)
            }
        }
    }
    
    @IBAction func loginPressed(_ sender: NSButton) {
    
        loginViewModel.getLogin(userName: self.lblUserName.stringValue, password: self.lblPassword.stringValue){(responseModel) in
            if(responseModel != nil){
                self.performCustomSegue(identifier: Constants.customSegueIdentifiers.dashboardViewSegue)
             }
        }
    }
}
