//
//  LoginViewModel.swift
//  WLA_RND_POC
//
//  Created by Taha Siddiqui on 3/7/19.
//  Copyright © 2019 Taha Siddiqui. All rights reserved.
//

import Foundation
import Core_Mac

class LoginViewModel :BaseViewModel {
    
    var client_id = Dynamic(0)
    var loginModel = Dynamic(LoginModel.self)
    func getLogin(userName:String,password:String,completion:@escaping (LoginModel?) -> Void){

        let userService = referenceContainer.resolve(UserServiceProtocol.self)
        userService?.login(userName:userName,password:password){(responseModel) in
        
            if let model = responseModel {
                print(model.client_id!)
               // completion(model)
               self.client_id.value = model.client_id!
             }
        }
    }
}
