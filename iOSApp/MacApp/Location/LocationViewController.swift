//
//  LocationViewController.swift
//  Presentation
//
//  Created by Taha Siddiqui on 3/21/19.
//  Copyright © 2019 Taha Siddiqui. All rights reserved.
//

import Cocoa

class LocationViewController: BaseViewController {

    var viewModel :LocationViewModel!

    @IBOutlet weak var labelCountry: NSTextField!
    @IBOutlet weak var labelISP: NSTextField!
    @IBOutlet weak var labelIP: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        
        viewModel = referenceContainer.resolve(LocationViewModel.self)
        
        //Binding of viewModel properties to UI Controls
        viewModel?.ip.bind{ (ip) in
            self.labelIP.stringValue = ip
        }
        
        viewModel?.isp.bind { (isp) in
            self.labelISP.stringValue = isp
            
        }
        
        viewModel?.country.bind{ (country) in
            self.labelCountry.stringValue = country
        }
    }
    
    @IBAction func getLocation(_ sender: Any) {
        
        viewModel.getCurrentLocation(id:0)
        
    }
    
}
