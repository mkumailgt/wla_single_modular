//
//  LocationViewModel.swift
//  Presentation
//
//  Created by Taha Siddiqui on 3/21/19.
//  Copyright © 2019 Taha Siddiqui. All rights reserved.
//

import Foundation
import Core_Mac

class LocationViewModel :BaseViewModel{
    
    var ip = Dynamic("")
    var country = Dynamic("")
    var isp = Dynamic("")
    
    func getCurrentLocation(id:Int) -> Void{
        
        let locationService = referenceContainer.resolve(LocationServiceProtocol.self)
        
        locationService?.getLocation(response: { (responseModel) in
            if let model = responseModel {
                self.country.value = model.country ?? ""
                self.ip.value = model.city ?? ""
                self.isp.value = model.isp ?? ""
            }
        })
//        locationService?.getLocation(id:id){ (responseModel) in
//            if let model = responseModel {
//                self.country.value = model.country ?? ""
//                self.ip.value = model.city ?? ""
//                self.isp.value = model.isp ?? ""
//            }
//        }
    }
}
