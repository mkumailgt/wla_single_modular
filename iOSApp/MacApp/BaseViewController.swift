//
//  BaseViewController.swift
//  Presentation
//
//  Created by Taha Siddiqui on 3/22/19.
//  Copyright © 2019 Taha Siddiqui. All rights reserved.
//

import Cocoa
import Core_Mac
import AppKit

class BaseViewController: NSViewController {

     let referenceContainer = (NSApplication.shared.delegate as! AppDelegate).dependencyContainer
    
    override func viewDidLoad() {
       
        
        let appDelegate = NSApplication.shared.delegate as! AppDelegate
        appDelegate.registerDependencies()
        
        performCustomSegue(identifier: Constants.customSegueIdentifiers.loginViewSegue)
        
    }
    
    func performCustomSegue(identifier:String){
        performSegue(withIdentifier: identifier, sender: self)
    }
}
