//
//  AppDelegate.swift
//  iOSApp
//
//  Created by Muhammad kumail on 26/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import UIKit
import Swinject
import SwinjectAutoregistration
import Core_iOS
import Network_iOS
import Service_iOS
import Repository_iOS
import AtomSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let dependencyContainer = Container()
    let atomConfig = AtomConfiguration()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        atomConfig.secretKey = "dab9cf530d2932f1bfe5572fcaa9fa3dc7bd32fb";
        atomConfig.vpnInterfaceName = "WLA";
        AtomManager.sharedInstance(with: atomConfig)
        AtomManager.sharedInstance()?.uuid = "mkumailgt@gmail.com"
        
        registerDependency()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func registerDependency() {
        
        dependencyContainer.autoregister(LocationNetworkProtocol.self, initializer: LocationNetwork.init)
        dependencyContainer.autoregister(LocationRepositoryProtocol.self, initializer: LocationRepository.init)
        dependencyContainer.autoregister(LocationServiceProtocol.self, initializer: LocationService.init)
        dependencyContainer.autoregister(UserNetworkProtocol.self, initializer: UserNetwork.init)
        dependencyContainer.autoregister(UserRepositoryProtocol.self, initializer: LoginRepository.init)
        dependencyContainer.autoregister(UserServiceProtocol.self, initializer: UserService.init)
        dependencyContainer.autoregister(UserViewModel.self, initializer: UserViewModel.init)
        dependencyContainer.autoregister(ServerListingViewModel.self, initializer: ServerListingViewModel.init)
        dependencyContainer.autoregister(VPNServiceProtocol.self, initializer: VPNManager.init)
        dependencyContainer.autoregister(ServerListingService.self, initializer: ServerListingService.init)
        dependencyContainer.autoregister(ServerListingRepositoryProtocol.self, initializer: ServerListingRepository.init)
        
    }
    
//    func getUUID() -> String {
//        let applicationUUID = UUID().uuidString
//        if (UserDefaults.standard.string(forKey: "UUID") == nil){
//            UserDefaults.standard.set(applicationUUID, forKey: "UUID")
//        }
//        return UserDefaults.standard.string(forKey: "UUID") ?? ""
//    }
}

