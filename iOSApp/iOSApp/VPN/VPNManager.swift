//
//  VPNManeger.swift
//  iOSApp
//
//  Created by Muhammad kumail on 28/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation
import Core_iOS
import UIKit
import AtomSDK
import ObjectiveC.runtime
import ObjectiveC.NSObjCRuntime
import CommonCrypto

public class VPNManager: VPNServiceProtocol {
    
    public func getCountries(response: @escaping ([CoutryModel]?) -> Void) {
        
        let test = AtomManager.sharedInstance()
        AtomManager.sharedInstance()?.getCountriesWithSuccess({ (allCountries) in
            
            var arrayCountries : Array<CoutryModel> = []
            
            for country in allCountries!
            {
                let countryMdl = CoutryModel()
                countryMdl.countryId = Int(country.countryId)
                countryMdl.name = country.name
                countryMdl.iso_code = country.iso_code
                arrayCountries.append(countryMdl)

            }
            response(arrayCountries)

        }, errorBlock: { (error) in
           
        })
        
             
    }
    
    public func getConnectedTime() {
        
    }
    
    public func getCountriesForSmartDialing()  {
        
    }
    
    public func getOptimizedCountries()  {
        
    }
    
    public func getConnectedIP() -> String {
        
        return ""
    }
//
    func getTypesOfProperties(in clazz: NSObject) -> Dictionary<String, Any>? {

        var dict : Dictionary<String,Any> = Dictionary <String ,Any> ()
        //var count = Int8()
        let count : UnsafeMutablePointer<UInt32>? = nil

        let properties  = class_copyPropertyList(clazz as? AnyClass, count)
       // let properties = class_getProperty(clazz as? AnyClass, count)

        // for i in 0..<Int(count) {
//        let property: objc_property_t = properties
//        let key : UnsafePointer<Int8> = property_getName(property)
//        let str = String(cString: key)
//        dict.updateValue(clazz .value(forKey: str) as Any, forKey: str)
        //}
       // free(properties);
        return dict
        //return [NSDictionary dictionaryWithDictionary:dict];
    }
    


}

