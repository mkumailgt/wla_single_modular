//
//  ServerListingViewModel.swift
//  iOSApp
//
//  Created by Muhammad kumail on 28/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation
import SimpleTwoWayBinding
import Core_iOS
import Service_iOS

class ServerListingViewModel: BaseViewModel {
   
    let dataModel : Observable<[CoutryModel]> = Observable()
    let lblCountry : Observable<String> = Observable()
    
    func getCountries()  {
        
        let serverListingService = refrenceContainer.resolve(ServerListingService.self)
        serverListingService?.getCountriesFromSDK(response: { (countryModel) in
            
            self.dataModel.value = countryModel
            
        })
        
    }
}
