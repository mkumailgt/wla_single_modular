//
//  LocationsViewController.swift
//  iOSApp
//
//  Created by Muhammad kumail on 28/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation
import UIKit
import SimpleTwoWayBinding
import Core_iOS
class ServerListingViewController: BaseViewController {
    
    
    @IBOutlet weak var tblView: UITableView!
    
    var serverListingViewModel : ServerListingViewModel!
    var serverListingCell = ConfigureCounriesCell()
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        serverListingViewModel =  refrenceContainer.resolve(ServerListingViewModel.self)
        bindViewModel()
        serverListingViewModel.getCountries()
        
        
        tblView.delegate = self
        tblView.dataSource = self

    }
    
    func bindViewModel() {
        serverListingViewModel.dataModel.bind { (data, countryModel) in
            self.tblView.reloadData()
        }
    }
}

extension ServerListingViewController : UITableViewDataSource , UITableViewDelegate
{
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.serverListingViewModel.dataModel.value?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : UITableViewCell  = tableView.dequeueReusableCell(withIdentifier: "serverLisitingCell", for: indexPath)
        let data = self.serverListingViewModel.dataModel.value
        if let data = data {
            cell = serverListingCell.setupCell(country:data[indexPath.row] )
        }
        
        return cell
    }
    
}
