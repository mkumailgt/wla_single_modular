//
//  ConfigureCounriesCell.swift
//  iOSApp
//
//  Created by Muhammad kumail on 30/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import UIKit
import Core_iOS
class ConfigureCounriesCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(country : CoutryModel) -> UITableViewCell {
        
        let cell = UITableViewCell()
        cell.backgroundColor = .clear
        cell.textLabel?.text = country.name
        cell.detailTextLabel?.text = String(country.countryId)
        cell.textLabel?.textColor = .white
        cell.detailTextLabel?.textColor = .white
        return cell
    }

}
