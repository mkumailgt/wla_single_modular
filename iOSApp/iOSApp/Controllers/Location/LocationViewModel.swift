//
//  LocationViewModel.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 04/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation
import SimpleTwoWayBinding
import Core_iOS

class LocationViewModel : BaseViewModel {
    
    let lblCountry : Observable<String> = Observable()
    let lblCity : Observable<String> = Observable()
    let lblIP : Observable<String> = Observable()
    let lblISP : Observable<String> = Observable()
    
    func getLocation(response : @escaping (LocationModelProtocol? , Error?) -> Void )
    {
        let locationService = refrenceContainer.resolve(LocationServiceProtocol.self)
        
        locationService?.getLocation(response: { (responseModel) in
            
            if let model = responseModel {
                self.lblCountry.value = model.country
                self.lblIP.value = model.ip
                self.lblISP.value = model.isp
                self.lblCity.value = model.city
            }
        })
    }
}
