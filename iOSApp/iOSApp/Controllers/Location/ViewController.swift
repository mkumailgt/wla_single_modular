//
//  ViewController.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 27/02/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {

    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblIsp: UILabel!
    @IBOutlet weak var lblIp: UILabel!
    
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    var locationViewModel = LocationViewModel()
    var loginViewModel : UserViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        loginViewModel = refrenceContainer.resolve(UserViewModel.self)
        locationViewModel.lblCountry.bind { (_ , countryName) in
            self.lblCountry.text = countryName
        }
        locationViewModel.lblCity.bind { (_ , cityName) in
            self.lblCity.text = cityName
        }
        locationViewModel.lblISP.bind { (_ , sourceISP) in
            self.lblIsp.text = sourceISP
        }
        locationViewModel.lblIP.bind { (_ , ipAddress) in
            self.lblIp.text = ipAddress
        }
        
    }

    @IBAction func btnGetLocation(_ sender: Any) {
        locationViewModel.getLocation { (locationData , error ) in

            if let error = error{
                print("Error : \(String(describing: error.localizedDescription))")
            }
        }
        
    }
    
    @IBAction func doLogin(_ sender: UIButton) {
        
        loginViewModel.login(userName: txtUserName.text!, password: txtPassword.text! , source: self)
    }
    
}

