//
//  LoginViewController.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 07/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation
import UIKit

class UserViewController: BaseViewController {
    
    @IBOutlet weak var lblClientId: UILabel!
    @IBOutlet weak var lblToken: UILabel!
    @IBOutlet weak var lblMcsStatus: UILabel!
    @IBOutlet weak var lblStatusCode: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblUuid: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let logingVM = refrenceContainer.resolve(UserViewModel.self)
        
        self.lblToken.text = logingVM?.token.value
        self.lblUuid.text = logingVM?.uuid.value
        self.lblStatus.text = logingVM?.isSync.value
    }
}
