//
//  SmartConnectViewModel.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 04/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation
import UIKit
import SimpleTwoWayBinding
import Core_iOS

class UserViewModel  {
 
    let clientId : Observable<Int> = Observable()
    let uuid : Observable<String> = Observable()
    let token : Observable<String> = Observable()
    let mcsStatus : Observable<String> = Observable()
    let isSync : Observable<String> = Observable()
    
    func login(userName : String , password : String , source: UIViewController )  {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let UserService = appDelegate.dependencyContainer.resolve(UserServiceProtocol.self)
        
        UserService?.login(userName : userName , password : password , response: { (responseData) in
            
            self.clientId.value = responseData?.client_id
            self.uuid.value = responseData?.uuid
            self.token.value = responseData?.token
            self.mcsStatus.value = responseData?.status_code
            self.isSync.value = responseData?.is_sync
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "loginVC") as? UserViewController
            source.navigationController?.pushViewController(vc!, animated: true)
            
        })
    }
    
    
}
