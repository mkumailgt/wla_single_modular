//
//  UserModel.swift
//  WLA_iOS
//
//  Created by Muhammad kumail on 20/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation
public struct UserProfileObjectModel :Codable{
    
    public let profile_data : UserProfileModel?
    public let active_addons : [Addons]?
    
    public init(profile_data: UserProfileModel,active_addons : [Addons]) {
        self.profile_data = profile_data
        self.active_addons = active_addons
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let profile_data = try container.decode(UserProfileModel.self, forKey: .profile_data)
        let active_addons = try container.decode([Addons].self, forKey: .active_addons)
        
        self.init(profile_data:profile_data,active_addons:active_addons)
    }
    
    private enum CodingKeys: String, CodingKey {
        case profile_data = "profile_data"
        case active_addons = "active_addons"
    }
}

public struct Addons :Codable{
    public let slug: String?
    public let title:String?
}

public struct UserProfileModel : Codable {
    
    public let uuid : String?
    public let client_id : Int?
    public let token : String?
    public let is_mp : String?
    public let is_sync : String?
    public let status_code : String?
    public let first_name : String?
    public let last_name : String?
    public let email :String?
    public let phone_number :String?
    public let mcs_status : Int?
    
    public var code : Int?
    public var message : String?
}

