//
//  LoctionModel.swift
//  WLA_iOS
//
//  Created by Muhammad kumail on 22/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

public class LocationModel : Decodable ,Encodable, LocationModelProtocol {
    
    public var Id: Int
   // public var success: Bool?
    public var ip: String?
    public var city: String?
    public var country: String?
    public var iso2: String?
    public var latitude: String?
    public var longituge: String?
    public var isp: String?
    public var organization: String?
    
    enum CodingKeys: String, CodingKey {
        
        case Id
       // case success
        case ip
        case city
        case country
        case iso2
        case latitude
        case longituge
        case isp
        case organization
    }
    
    required public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        Id = 0
        //success = try container.decode(Bool.self, forKey: .success)
        ip = try container.decode(String.self, forKey: .ip)
        city = try container.decode(String.self, forKey: .city)
        country = try container.decode(String.self, forKey: .country)
        iso2 = try container.decode(String.self, forKey: .iso2)
        latitude = try container.decode(String.self, forKey: .latitude)
        longituge = try container.decode(String.self, forKey: .longituge)
        isp = try container.decode(String.self, forKey: .isp)
        organization = try container.decode(String.self, forKey: .organization)
        
    }
}
