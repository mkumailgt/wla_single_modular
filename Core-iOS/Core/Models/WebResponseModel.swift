//
//  WebResponseModel.swift
//  WLA_RND_POC
//
//  Created by Taha Siddiqui on 3/22/19.
//  Copyright © 2019 Taha Siddiqui. All rights reserved.
//

import Foundation

public struct WebResponseModel {
    
    /// The URL request sent to the server.
    public let request: URLRequest?
    
    /// The server's response to the URL request.
    public let response: HTTPURLResponse?
    
    /// The data returned by the server.
    public let data: Data?

    public let result : Dictionary<String ,Any>
    
    public let isSuccess :Bool?
    
    public let isFailure :Bool?
    
    public var error: Error?
    
    
}

