//
//  SmartConnectNetworkModel.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 04/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

public struct LoginModel :Codable{
    public let uuid : String?
    public let client_id : Int?
    public let token : String?
    public let is_mp : String?
    public let is_sync : String?
    public let status_code : String?
    public var message :String?
    public var code :Int?
}

