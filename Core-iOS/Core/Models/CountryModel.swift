//
//  CountryModel.swift
//  Core-iOS
//
//  Created by Muhammad kumail on 28/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

public class CoutryModel : Decodable , Encodable , CountryModelProtocol {
    
    
    public var countryId: Int = 0
    public var name: String? = ""
    public var iso_code: String? = ""
      
    enum CodingKeys: String, CodingKey {

        case countryId
        case name
        case iso_code
    }

    required public init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)

        countryId = try container.decode(Int.self, forKey: .countryId)
        name = try container.decode(String.self, forKey: .name)
        iso_code = try container.decode(String.self, forKey: .iso_code)
    }
    public init(){
        return
    }
}
