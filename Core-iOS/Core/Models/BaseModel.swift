//
//  BaseModel.swift
//  WLA_iOS
//
//  Created by Muhammad kumail on 22/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

public protocol BaseModel {
    func mapingModel<T>() -> T
}
