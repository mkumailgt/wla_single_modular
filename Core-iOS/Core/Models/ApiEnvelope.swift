//
//  ApiReplyModel.swift
//  WLA_POC
//
//  Created by Gaditek on 06/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

public struct ApiEnvelope<T: Codable> : Codable {
    
    public var isRequestSuccess : Bool?
    public var header : HeaderModel?
    public let body : T?

}

