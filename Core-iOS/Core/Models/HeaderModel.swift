//
//  HeaderModel.swift
//  WLA_POC
//
//  Created by Gaditek on 06/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

public struct HeaderModel: Codable {
    
    public var code : Int
    public let message : String?
    
    public init(code: Int, message: String) {
        self.code = code
        self.message = message
    }
    
    public init(from decoder: Decoder) throws {
        var code :Int = 0
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let codeString =  try? container.decode(String.self, forKey: .code){
            code = Int(codeString) ?? 0
        }else if let codeNumber = try? container.decode(Int.self, forKey: .code){
            code = codeNumber
        }
        let message = try container.decode(String.self, forKey: .message)
        
        self.init(code:code, message:message)
    }
    
    private enum CodingKeys: String, CodingKey {
        case code = "code"
        case message = "message"
    }
}
