//
//  ApiUrls.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 3/14/19.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

public struct ApiAllUrls{
    public static var URL_LOCATION = "https://dialerxn.purevpn.net/dialer/select-location/Dialer_XMLS/ip_location.php?method=json"
    public static var URL_LOGIN = "https://api.dialertoserver.com/user/login.json?api_key=pvpnUserPrd"
    public static var URL_GET_USER = "https://api.dialertoserver.com/user/getProfile.json?api_key=pvpnUserPrd"
    public static var BASE_URL_FILE = String(format:"https://s3.amazonaws.com/purevpn-dialer-assets/%@/",Utilities.deviceType ?? "")
    public static var API_KEY = "api_key=pvpnUserPrd"
    public static var BASE_URL_API = "base_url"
    public static var BASE_URL_PLATFORM = "base_url_platform"
    public static var BASE_URL_DIALERXN = "base_url_dialerxn"
    public static var BASE_URL_DIALERXN_DIALER_TO_SERVER = "base_url_dialerxn_dialertoserver"
    public static var BASE_URL_AUTOLOGIN = "base_url_autologin"
    public static var BASE_URL_AUTOLOGIN_STAGING = "base_url_autologin_staging"
    public static var USER_GET_PROFILE = "user_getProfile"
    public static var USER_CREATE_TICKET_IN_ZENDESK = "user_createTicketInZendesk"
    public static var USER_ISMP = "user_isMp"
    public static var USER_LOGIN = "user_login"
    public static var USER_GET_BANDWITDH = "user_getBandwidth"
    public static var USER_RESEND_VERIFICATION_CODE = "user_resendVerificationCode"
    public static var USER_VERIFY_ACCOUNT = "user_verifyAccount"
    public static var USER_SIGNUP = "user_signup"
    public static var USER_SIGNUP_WITH_GOOGLE_PLUS = "user_signupWithGooglePlus"
    public static var USER_SIGNUP_WITH_FACEBOOK = "user_signupWithFacebook"
    public static var USER_SAVE_ERROR = "user_saveError"
    public static var USER_SAVE_FEEDBACK = "user_saveFeedback"
    public static var USER_FORGOT_VPN_PASSWORD = "user_forgotVpnPassword"
    public static var USER_AUTO_CMS_LOGIN = "user_autoCMSLogin"
    public static var USER_SAVE_CONNECTION_SPEED = "user_saveConnectionSpeed"
    public static var USER_MIXPANEL_REMEDY = "user_mixpanelRemedy"
    public static var USER_ADD_SOCIAL_ACTIVITY = "user_addSocialActivity"
    public static var USER_ADD_PURE_DNS_RULE = "user_addPureDnsRule"
    public static var PAYMENT_DO_PAYMENT = "payment_doPayment"
    public static var PAYMENT_HANDLE_PAYLOADER = "payment_handlePayLoader"
    public static var CART_GET_CURRENT_PRICING = "cart_getCurrentPricing"
    public static var EVENTS_GET_ACTIVE_EVENTS = "events_getActiveEvents"
    public static var PLATFORM_SPEEDTEST_GETSERVERS_WITHOUT_PSK = "platform_speedtest_getServersWithoutPsk"
    public static var PLATFORM_SPEEDTEST_GETSERVERS = "platform_speedtest_getServers"
    public static var DIALERXN_SPEEDTEST = "dialerxn_speedtest"
    public static var DIALERXN_IP_LOCATION = "dialerxn_ip_location"
    public static var DIALERXN_DIALERTOSERVER_IP_LOCATION = "dialerxn_dialertoserver_ip_location"
    public static var DIALERXN_CONNECTED_DIALERTOSERVER_IP_LOCATION = "dialerxnconnected_dialertoserver_ip_location"
    public static var INVENTORY_GETSMARTCONNECT = "inventory_getSmartConnect"
    public static var INVENTORY_GETSMARTCONNECTBY_RESELLER_ID = "inventory_getSmartConnectByResellerId"
    public static var INVENTORY_GET_ALL_DNS_KEYS = "inventory_getAllDnsKeys"
    public static var CONFIGURE_PUREDNS_REDIRECT_URL = "configure_puredns_redirect_url"
    public static var INVENTORY_ADD_SMART_ADVANCE_FEATURES = "inventory_addSmartAdvanceFeatures"
    public static var APPROLLOUT_GETVERSION = "approllout_getVersion"
    
}
