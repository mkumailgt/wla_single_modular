//
//  CommonRequestMethods.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 11/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation
import Alamofire
import CommonCrypto

let SaltPureVpn = "*PVPN123#"

public class Utilities: NSObject {
    
   public struct ApiMethods {
        
        static let options = "OPTIONS"
        static let get     = "GET"
        static let head    = "HEAD"
        static let post    = "POST"
        static let put     = "PUT"
        static let patch   = "PATCH"
        static let delete  = "DELETE"
        static let trace   = "TRACE"
        static let connect = "CONNECT"
    }
    
    public static var deviceId : String? {
        set{
            var deviceid : String = ""
            #if os(iOS)
                deviceid = UIDevice.current.identifierForVendor?.uuidString ?? ""
            #else
                deviceid = Utilities.macSerialNumber
            #endif
            
            UserDefaults.standard.set(deviceid, forKey:Constants.UserDefaults.Device_ID)
        }
        get {
            guard let deviceid = UserDefaults.standard.object(forKey: Constants.UserDefaults.Device_ID) as? String else{
                
                print("Device Id not set")
                //let deviceid = UIDevice.current.identifierForVendor?.uuidString
                UserDefaults.standard.set("", forKey:Constants.UserDefaults.Device_ID)
                return ""
            }
            return deviceid
        }
    }

    
  public  static var deviceType : String? {
        get{
            #if os(iOS)
            return "ios"
            #elseif os(watchOS)
            return "watchOS"
            #elseif os(tvOS)
            return "tvOS"
            #elseif os(OSX)
            return "mac"
            #else
            println("OMG, it's that mythical new Apple product!!!")
            #endif
        }
    }
    
   public static var mcsStatusCode: Int{
        
        set (mcsStatusCode){
            UserDefaults.standard.set(mcsStatusCode, forKey: Constants.UserDefaults.McsStatus)
        }
        get{
            let statusCode = UserDefaults.standard.object(forKey: Constants.UserDefaults.McsStatus)
            return statusCode as! Int
        }
    }
    
 public   static var mcsStatus :String? {
        
        set{
            UserDefaults.standard.set(mcsStatus, forKey: Constants.UserDefaults.McsStatus)
        }
        get{
            let status = UserDefaults.standard.object(forKey: Constants.UserDefaults.McsStatus)
            return status as? String
        }
    }
    
  public  static var accessToken: String? {
        
        set(accessToken){
            UserDefaults.standard.set(accessToken, forKey: Constants.UserDefaults.AccessToken)
        }
        get{
            let accessToken = UserDefaults.standard.object(forKey: Constants.UserDefaults.AccessToken)
            return accessToken as? String ?? ""
        }
    }
    
  public  static var clientId: Int{
        
        set(clientId){
            UserDefaults.standard.set(clientId, forKey: Constants.UserDefaults.ClientId)
        }
        get{
            let clientId = UserDefaults.standard.object(forKey: Constants.UserDefaults.ClientId)
            return clientId as! Int
        }
        
    }
    
  public  static var userName: String?{
        set(userName){
            UserDefaults.standard.set(userName, forKey: Constants.UserDefaults.Username)
        }
        get{
            let userName = UserDefaults.standard.object(forKey: Constants.UserDefaults.Username)
            return userName as? String
        }
        
    }
    
 public   static var emailAddress: String? {
        set(emailAddress){
            UserDefaults.standard.set(emailAddress, forKey: Constants.UserDefaults.Email_Address)
        }
        get{
            let emailAddress = UserDefaults.standard.object(forKey: Constants.UserDefaults.Email_Address)
            return emailAddress as? String
        }
        
    }
    
 public   static var uuid : String? {
        set(uuid){
            UserDefaults.standard.set(uuid, forKey: Constants.UserDefaults.Uuid)
        }
        get{
            let uuid = UserDefaults.standard.object(forKey: Constants.UserDefaults.Uuid)
            return uuid as? String
        }
    
    }
    
  public  static var mixPanelEventsArray : Array<Any> {
        set(mixPanelEventsArray){
            UserDefaults.standard.set(mixPanelEventsArray, forKey: Constants.UserDefaults.MP_Events_Array)
        }
        get{
            let MpEventsArray = UserDefaults.standard.object(forKey: Constants.UserDefaults.MP_Events_Array)
            return (MpEventsArray as? Array<Any>)!
        }
    }

    
 public   static var dateTime : String? {
        
        get{
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            let result = formatter.string(from: date)
            return result
        }
    }
    
  public  static var locale: String?{
        get{
        return "en"
        }
    }
    
  public  static var buildVersion : String? {
        get{
            let dictionary = Bundle.main.infoDictionary!
            let version = dictionary["CFBundleShortVersionString"] as! String
            return "\(version)"
        }
    }
    
 public   static var signature: String? {
        get{
            let signature : String = Utilities.dateTime! + SaltPureVpn
            let mdf5String: String = signature.md5()
            return mdf5String
        }
    }
    
 public   static var md5Token: String? {
        get{
            let str = String(format: "%d",  Utilities.clientId )
            let signature : String = str+SaltPureVpn
            let mdf5String: String = signature.md5()
            return mdf5String
        }
    }
    
 public  static var getCommonProperties: [String:Any] {
        
        get{
            var commonParamters:[String:Any] = [String:Any]()
            
            if let userName = Utilities.userName{
                commonParamters.updateValue(userName, forKey: Constants.CommonParameters.Username)
            }
            else {
                print("userName Not Set")
            }
            
            if let deviceId = deviceId {
                commonParamters.updateValue(deviceId, forKey: Constants.CommonParameters.DeviceId)
            } else {
                print("Device id Not Set")
                
            }
            
            if let deviceType = deviceType{
                commonParamters.updateValue(deviceType, forKey: Constants.CommonParameters.DeviceType)
            }
            
            if let dateTime = Utilities.dateTime{
                commonParamters.updateValue(dateTime, forKey: Constants.CommonParameters.DateTime)
            }
            
            if let token = Utilities.accessToken{
                
                commonParamters.updateValue(token, forKey: Constants.CommonParameters.Token)
            }
            
            if let signature = Utilities.signature{
                commonParamters.updateValue(signature, forKey: Constants.CommonParameters.Signature)
            }
            
            commonParamters.updateValue(1, forKey: Constants.CommonParameters.resellerId)
            //        commonParamters.updateValue(1, forKey: keys.CommonParameters.iMcs)
            
            if let language = Utilities.locale{
                commonParamters.updateValue(language, forKey: Constants.CommonParameters.Locale)
            }
            
            return commonParamters
        }
    }

    public static var macSerialNumber :String{
        get{
            #if os(iOS)
                return ""
            #else
            
            let platformExpert: io_service_t = IOServiceGetMatchingService(kIOMasterPortDefault, IOServiceMatching("IOPlatformExpertDevice"));
            let serialNumberAsCFString = IORegistryEntryCreateCFProperty(platformExpert, kIOPlatformSerialNumberKey as CFString, kCFAllocatorDefault, 0)
            IOObjectRelease(platformExpert);
            
            return (serialNumberAsCFString!.takeUnretainedValue() as? String) ?? ""
            #endif
        }
    }
}



extension String {
    /**
     Get the MD5 hash of this String
     
     - returns: MD5 hash of this String
     */
    
  public  func md5() -> String! {
        let context = UnsafeMutablePointer<CC_MD5_CTX>.allocate(capacity: 1)
        var digest = Array<UInt8>(repeating:0, count:Int(CC_MD5_DIGEST_LENGTH))
        CC_MD5_Init(context)
        CC_MD5_Update(context, self, CC_LONG(self.lengthOfBytes(using: String.Encoding.utf8)))
        CC_MD5_Final(&digest, context)
        context.deallocate()
        var hexString = ""
        for byte in digest {
            hexString += String(format:"%02x", byte)
        }
        
        return hexString
    }
}
