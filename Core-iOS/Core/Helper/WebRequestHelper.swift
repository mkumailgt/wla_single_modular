//
//  NetworkHelper.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 27/02/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation
import Alamofire

typealias CompletionHandler = (DataResponse<Data>) -> Void
typealias requestCompletionHandler<T:Codable> = (ApiEnvelope<T>?,Swift.Error?) -> Void

public class WebRequestHelper {
    
    static let alamofireManager: SessionManager = {
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.timeoutIntervalForRequest = 50
        //sessionConfiguration.httpAdditionalHeaders = ["X-Psk": "FGtbAXMT4QBBLQj97jvP"]
        return Alamofire.SessionManager(configuration: sessionConfiguration)
    }()
    
    public class func get(url:String,parameters:[String:Any]?,headers:[String:String]?,completionHandler:@escaping (WebResponseModel) -> Void){
        
        let requestOperation = WebRequestHelper.alamofireManager.request(url, method: HTTPMethod.get, parameters: parameters, headers: headers)
        
        requestOperation.responseJSON { (responseData) in
            
            let responseModel = WebResponseModel(request: responseData.request, response: responseData.response, data: responseData.data, result: responseData.result.value as! Dictionary<String, Any>, isSuccess: responseData.result.isSuccess, isFailure: responseData.result.isFailure, error: responseData.error)
            
            completionHandler(responseModel)
        }
    }
    
    public  class func post(url:String,parameters:[String:Any]?,headers:[String:String]?,completionHandler:@escaping (WebResponseModel) -> Void){
        
        let requestOperation = WebRequestHelper.alamofireManager.request(url, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        
        requestOperation.responseJSON { (responseData) in
            
            let responseModel = WebResponseModel(request: responseData.request, response: responseData.response, data: responseData.data, result: responseData.result.value as! Dictionary<String, Any>, isSuccess: responseData.result.isSuccess, isFailure: responseData.result.isFailure, error: responseData.error)
            
            completionHandler(responseModel)
        }
    }
}
