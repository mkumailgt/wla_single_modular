//
//  CommonConstants.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 11/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

public struct Constants {
    
    public struct UserDefaults {
        
        public static let Device_ID    = "default_device_id"
        public static let Signature    = "default_signature"
        public static let AccessToken  = "default_access_token"
        public static let Username     = "default_username"
        public static let Password     = "default_password"
        public static let McsStatus    = "default_mcs_code"
        public static let ClientId     = "default_client_id"
        public static let Uuid         = "default_uuid"
        public static let Email_Address  = "default_email_eddress"
        public static let MP_Events_Array  = "default_mp_events_array"
        
    }
    
    public struct CommonParameters {
        
        public static let DeviceId = "sDeviceId"
        public static let DeviceToken = "sDeviceToken"
        public static let DeviceType = "sDeviceType"
        public static let DateTime = "sDateTime"
        public static let Signature = "sSignature"
        public static let Extras = "sExtras"
        public static let Username = "sUsername"
        public static let Password = "sPassword"
        public static let Message = "sMessage"
        public static let ClientId = "iClientId"
        public static let Token = "sToken"
        public static let isTrial = "iTrial"
        public static let AccessToken = "sAccessToken"
        public static let Email = "sEmail"
        public static let Locale = "sLocale"
        public static let VpnUsername = "sVpnUsername"
        public static let Icode = "iCode"
        public static let App_version = "app_version"
        public static let resellerId = "iResellerId"
        public static let IStatus = "iStatus"
    }
    
    public struct FeedbackParameters {
        
        public static let AnswerId = "iAnswerId"
        public static let PolicyId = "iPolicyId"
        public static let QuestionId = "iQuestionId"
        public static let Answer = "sAnswer"
        public static let Additional_comments = "additional_comments"
        public static let Country_code = "country"
        public static let Device_manufacture = "device_manufacture"
        public static let Device_name = "device_name"
        public static let IpAddress = "ip"
        public static let Language_code = "language_code"
        public static let Operating_System = "operating_system"
        public static let OS_Version = "os_version"
        public static let Rating = "rating"
        public static let Selected_Mode_Id = "selected_mode_id"
        public static let Selected_Mode_Name = "selected_mode_name"
        public static let Selected_Purpose_Id = "selected_purpose_id"
        public static let Selected_Purpose_Name = "selected_purpose_name"
        public static let ExtraInfo = "sExtraInfo"
        
    }
    
    public struct DoPaymentParameters {
        
        public static let TransId = "sTransId"
        public static let Amount  = "dAmount"
        public static let Gateway = "sGateway"
        public static let Appstore = "appstore"
        public static let DumpData = "sDumpData"
        public static let BillingCycle = "sBillingCycle"
        public static let ProductId = "sProductId"
        public static let Extra = "sExtra"
        public static let Version = "sVersion"
    }
    
    public struct ApiMethods{
        
        public static let options = "OPTIONS"
        public static let get     = "GET"
        public static let head    = "HEAD"
        public static let post    = "POST"
        public static let put     = "PUT"
        public static let patch   = "PATCH"
        public static let delete  = "DELETE"
        public static let trace   = "TRACE"
        public static let connect = "CONNECT"
    }
    
    public struct AdditionalHeaders {
        
        public static let XPSK = "FGtbAXMT4QBBLQj97jvP"
        public static let HTTP_AGENT_IOS =  "purevpn-http-agent-ios"
    }
    
    public struct HeaderSuccessCodes {
        public static let zero = 0
        public static let one = 1
    }
    
    public struct customSegueIdentifiers{
        
        public static let mainViewSegue = "customCheckDashboard"
        public static let loginViewSegue = "loginSegue"
        public static let dashboardViewSegue = "dashboardSegue"
        
    }
    
}
