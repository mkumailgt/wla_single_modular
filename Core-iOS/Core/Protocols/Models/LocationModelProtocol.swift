//
//  LocationNetworkModel.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 27/02/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

public protocol LocationModelProtocol {
    
    var Id: Int {get set}
  //  var success: Bool? {get set}
    var ip : String? {get set}
    var city : String? {get set}
    var country : String? {get set}
    var iso2 : String? {get set}
    var latitude : String? {get set}
    var longituge : String? {get set}
    var isp : String? {get set}
    var organization : String? {get set}
}

