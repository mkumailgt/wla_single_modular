//
//  CountryModelProtocol.swift
//  Core-iOS
//
//  Created by Muhammad kumail on 28/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

public protocol CountryModelProtocol {
    
    var countryId: Int {get set}
    var name: String? {get set}
    var iso_code: String? {get set}
   // var latitude: String? {get set}
    //var longitude: String? {get set}
   // var protocols: Array<Any>? {get set}
   // var dataCenters: Array<Any>? {get set}
   // var latency: Int? {get set}
    //var isSmartDialingSupported: Bool? {get set}
}
