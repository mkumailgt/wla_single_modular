//
//  BaseNetworkProtocol.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 06/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

public protocol BaseNetworkProtocol {
    
    var apiUrl : String {get set}
    var apiEndPoint : String {get set}
    var apiHttpBody : String {get set}
    var apiParameters:[String:Any] {get set}
    var apiHttpHeader : [String:String]? {get set}
    var apiHttpResponse : String {get set}
    var apiHttpMethod : String {get set}
    var apiHttpResponseCode : Int {get set}
    var apiErrorCode : String {get set}
    var apiErrorMessage : String {get set}
    var successCode : Int {get set}
}
