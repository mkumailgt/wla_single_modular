//
//  SmartConnectNetworkProtocol.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 04/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

public protocol UserNetworkProtocol : BaseNetworkProtocol {
    func login(paramters:[String:Any],response : @escaping(LoginModel?) -> Void)
    func getUserProfile(paramters:[String:Any],response :@escaping(UserProfileModel?) -> Void)
}
