//
//  LocationNetworkProtocol.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 27/02/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

public protocol LocationNetworkProtocol : BaseNetworkProtocol {
    
    func getLocation(parameters:[String:Any]?,response: @escaping (LocationModel?) ->  Void)
}
