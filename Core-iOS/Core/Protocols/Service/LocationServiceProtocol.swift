//
//  LocationServiceProtocol.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 27/02/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

public protocol LocationServiceProtocol {
    
    func getLocation(response:@escaping (_ response:LocationModel?) -> Void)
}

