//
//  VPNServiceProtocol.swift
//  Core-iOS
//
//  Created by Muhammad kumail on 28/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

public protocol VPNServiceProtocol {
    
    func getCountries(response:@escaping (_ response:[CoutryModel]?) -> Void)
    func getConnectedTime()
    func getCountriesForSmartDialing()// -> [CoutryModel]
    func getOptimizedCountries() //-> [CoutryModel]
    func getConnectedIP() -> String  
}
