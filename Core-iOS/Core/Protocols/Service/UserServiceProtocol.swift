//
//  SmartConnectServiceProtocol.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 04/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

public protocol UserServiceProtocol {
    
    func getUserProfile(completion:@escaping (UserProfileModel?) -> Void)
    func login(userName:String,password:String,response : @escaping(LoginModel?) -> Void)
}
