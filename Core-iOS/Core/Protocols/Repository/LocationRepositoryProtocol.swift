//
//  LocationRepositoryProtocol.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 27/02/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

public protocol LocationRepositoryProtocol {
    
    func getLocationById(Id:Int) -> LocationModel?
    func addlocation (object : LocationModel)
}
