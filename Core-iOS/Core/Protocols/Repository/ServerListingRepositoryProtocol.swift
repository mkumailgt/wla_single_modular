//
//  ServerListingRepositoryProtocol.swift
//  Core-iOS
//
//  Created by Muhammad kumail on 29/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

public protocol ServerListingRepositoryProtocol {
    
    func getCountries() -> [CoutryModel]?
    func addCountries (object : [CoutryModel])
}
