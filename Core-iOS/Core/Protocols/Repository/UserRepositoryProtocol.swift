//
//  SmartConnectRepositoryPorotocol.swift
//  WLA_POC
//
//  Created by Muhammad kumail on 04/03/2019.
//  Copyright © 2019 Muhammad kumail. All rights reserved.
//

import Foundation

public protocol UserRepositoryProtocol {
    
    func getLoginFromDB () //-> [SmartConnectRepositoryModel]
    func addLoginDataToDb (object : AnyObject)
}
